/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.tplsofteng.nbgolang.core;

/**
 * @author tplsofteng
 */
public class Globals {

    public static final String PROJECT_TYPE = "org-golang-project";
    public static final String GOLANG_MIME_TYPE = "text/x-go";
    public static String GOLANG_GOPATH_VAR = "GOPATH";
}
