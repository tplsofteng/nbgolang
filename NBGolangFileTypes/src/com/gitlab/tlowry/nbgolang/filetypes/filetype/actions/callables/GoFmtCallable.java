/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.tplsofteng.nbgolang.filetypes.filetype.actions.callables;

import java.util.concurrent.Callable;

/**
 *
 * @author tplsofteng
 */
public class GoFmtCallable implements Callable<Object>{
    private final String filePaths;
    
    public GoFmtCallable(String spacedFilePaths){
        this.filePaths = spacedFilePaths;
    }

    @Override
    public Object call() throws Exception {
        return new ProcessBuilder("gofmt", "-w", filePaths).start();
    }
    
}
